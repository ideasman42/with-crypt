
##########
With-Crypt
##########

Convenient, temporarily access to password protected files & archives.

This utility was created after finding most encryption tools unwieldy and complicated.

With-crypt provides a convenient way to access encrypted files without the need to manually perform multiple steps
to create, access & securely remove data.

There are many resources for how you can use commands to encrypt/decrypt files.
This is OK but impractical for quick access.


Motivation
==========

As there are so many solutions for encryption, any new tool should justify it's existence.

Existing tools seem to fall under various categories.

- Comprehensive tools that require user **buy-in**

  Users must install the tools and use them order to access their data.
  This has the down-side of risking your data stored in an application that becomes unmaintained.

  By comparison **with-crypt** is only a convenience wrapper around existing commands and does nothing special.
  You can relatively easily decrypt the files manually, should you need to.

- Highly Integrated

  There are plug-ins for editors that support decryption files on read/write,
  but the integration for storing secrets and to configure the exact methods of encrypting/decrypting
  gets involved.

- Knowledge of Underlying Technologies

  Many tools require users to be interested and become knowledgeable in the underlying technologies.
  While there is nothing wrong with this, they tend to require users take an interested in encryption.


Usage
=====

- Decrypted files are extracted into the temporary directory for viewing/editing.
- Accessing encrypted files is temporary, afterwards they are securely removed.
- Accessing encrypted files is bound to the lifetime of the the command run, defined by trailing arguments.
- The password must be set using the environment variable ``PASSWORD``.
- All encrypted files must use the ``.enc`` extension.


Best Practice
-------------

Passwords
   The password should not be stored in plain-text on the system,
   instead it is recommended that it be stored on a security token or typed in manually.

   This example show how to use text input to get the secret:
   ``PASSWORD=$(systemd-ask-password "Enter your password:")``.

   This example shows how a secret from a NitroKey can be used, which requires physically pressing on the NitroKey:
   ``PASSWORD=$(nitropy nk3 secrets get-password MY_SECRET_NAME --password)``.


In Memory Editing
   You may wish to temporary edit the files in memory (RAM), so the files are never written to persistent-storage.
   This can be done by setting the temporary directory (``TMPDIR`` on Unix-like systems).

   .. code-block:: sh

      env TMPDIR=/dev/shm PASSWORD=MY_PASSWORD with-crypt example.txt.enc nano {}


   To ensure the data is never written unencrypted to persistent-storage,
   take care with swap memory, either:

   - Encrypted the swap partition.
   - Mount ``tmpfs`` with ``noswap`` enabled.


Modes
-----

There are two modes of accessing encrypted files.

Single File
   Supports accessing a single, password protected file.
Archive
   Supports extracting an entire archive into a temporary directory
   where files may be manipulated and re-packed into the archive.


Archives
--------

The following file extensions will automatically be detected as archives.

- ``.tar`` for an uncompressed archive.
- ``.tar.bz2``, ``.tbz2`` for BZIP2.
- ``.tar.gz``, ``.tgz`` for GZIP.
- ``.tar.xz``, ``.txz`` for XZ.

When encrypted, these will have an additional extension, e.g. ``archive.tgz.enc``.


Examples
--------

Note that the following examples assume a ``PASSWORD`` environment variable is set.

Encrypt a text file
   .. code-block:: sh

      with-crypt --file=example.txt.enc --create-from=/path/to/text.txt

View an encrypted text file
   .. code-block:: sh

      with-crypt --file=example.txt.enc --read-only cat {}

Edit an encrypted text file
   .. code-block:: sh

      with-crypt --file=example.txt.enc nano {}

Encrypt a directory into an archive
   .. code-block:: sh

      with-crypt --file=my_vault.tar.gz.enc --create-from=/path/to/directory

Open a shell in encrypted archive.
   .. code-block:: sh

      with-crypt --file=my_vault.tar.gz.enc --cwd $SHELL

Convenience Aliases
   Example of shell-aliases to view & edit a ``secrets.txt`` file.

   .. code-block:: sh

      alias my-secret-view='env \
        TMPDIR=/dev/shm \
        PASSWORD=$(nitropy nk3 secrets get-password MY_SECRET_NAME --password) \
        with-crypt \
          --file ~/secrets.txt.enc \
          --no-secure-delete \
          --read-only \
          nano --view {}'

      alias my-secret-edit='env \
        TMPDIR=/dev/shm \
        PASSWORD=$(nitropy nk3 secrets get-password MY_SECRET_NAME --password) \
        with-crypt \
          --file ~/secrets.txt.enc \
          --no-secure-delete \
          nano {}'


Help Text
=========

For typical usage you should not be entering a long list of command-line arguments,
nevertheless there are times where you may want to change the default behavior.

.. BEGIN HELP TEXT

Output of ``with-crypt --help``

usage::

       with-crypt [-h] -f FILE [-I REPLACE] [-r] [--cwd] [--create]
                  [--create-from CREATE_FROM_PATH] [--no-secure-delete]
                  [--mode MODE] [--backend BACKEND]
                  ...

A simple utility to decrypt a file, edit it, then save encrypted.

environment variables:
  :PASSWORD:
     Must be set to the password used to encrypt and decrypt the archive.
  :PASSWORD_OLD:
     May be set if you wish to change the password for an existing file.
     The old password is used to decrypt and ``PASSWORD`` is used to encrypt.

positional arguments:
  :COMMAND:
                        The command to operate on the file. The value if ``REPLACE``
                        will be replaced with the file name or the archive directory-name.

                        The command can only be omitted if ``--create`` or ``--create-from`` are used.

                        Note that this command must block as the file will be re-encrypted
                        (unless ``--read-only`` is passed in).

options:
  -h, --help
                        show this help message and exit
  -f, --file FILE
                        The path to the encrypted file (ending with an ``.enc``).
  -I, --replace REPLACE
                        Text in the COMMAND which will be replaced with the decrypted FILE.

                        - For single files the temporary decrypted file name is used.
                        - For archives the temporary decrypted directory is used.

                        The default value is ``{}``.
  -r, --read-only
                        Don't re-encrypt the file.
  --cwd
                        Change the working directory to the location of the decrypted files when running commands.

                        This is mainly useful for archives, where you may for example,
                        launch a $SHELL at the location of the extracted files.
  --create
                        Create a new empty file or archive, PATH must not yet exist.
  --create-from CREATE_FROM_PATH
                        Select a source for the creation data (a file or a directory for an archive).
                        This implies ``--create`` without the need to pass it in.

                        Trailing commands aren't required when this argument is passed in,
                        allowing file encryption without performing any other actions.
  --no-secure-delete
                        When passed in, secure delete is not used.
                        You may wish to use this when using an in-memory file-system.
  --mode MODE
                        Select the mode for extracting encrypted contents.

                        - ``AUTO`` select the mode based on the file extension.
                        - ``FILE`` treat the path as a single file to decrypt.
                        - ``ARCHIVE`` extract the file as an archive (failing when the extension is not known).
  --backend BACKEND
                        Select the backend used for encrypting & decrypting files.

                        - ``OPENSSL`` uses OpenSSL (default).
                        - ``GPG`` uses GnuPG.

.. END HELP TEXT
